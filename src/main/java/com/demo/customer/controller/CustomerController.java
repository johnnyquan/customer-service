package com.demo.customer.controller;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import com.demo.customer.dao.entity.Customer;
import com.demo.customer.dto.request.CustomerRequest;
import com.demo.customer.dto.request.IdentifiedCustomerRequest;
import com.demo.customer.dto.response.IdResponse;
import com.demo.customer.service.CustomerService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/customer")
public class CustomerController {

    private final CustomerService customerService;

    public CustomerController(final CustomerService customerService) {
        this.customerService = customerService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public IdResponse create(@RequestBody @Valid @NotNull final CustomerRequest request) {
        return customerService.create(request);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@RequestBody @Valid @NotNull final IdentifiedCustomerRequest identifiedCustomerRequest) {
        customerService.update(identifiedCustomerRequest);
    }

    @DeleteMapping("/{customerId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable @NotBlank final String customerId) {
        customerService.delete(customerId);
    }

    @GetMapping("/search/{firstNameOrLastName}")
    public List<Customer> search(@PathVariable @NotBlank final String firstNameOrLastName) {
        return customerService.search(firstNameOrLastName);
    }

    @GetMapping("/list")
    public List<Customer> getAll() {
        return customerService.getCustomers();
    }
}