package com.demo.customer.service;

import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.demo.customer.dao.entity.Customer;
import com.demo.customer.dao.repository.CustomerRepository;
import com.demo.customer.dto.converter.CustomerDtoMapper;
import com.demo.customer.dto.request.CustomerRequest;
import com.demo.customer.dto.request.IdentifiedCustomerRequest;
import com.demo.customer.dto.response.IdResponse;
import org.apache.lucene.search.Query;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CustomerService {

    private final CustomerRepository customerRepository;

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    public CustomerService(final CustomerRepository customerRepository){
        this.customerRepository = customerRepository;
    }

    public IdResponse create(final CustomerRequest customerRequest) {
        final String uuid = UUID.randomUUID().toString();
        final Customer customer=  Customer.builder()
                .id(uuid)
                .firstName(customerRequest.getFirstName())
                .lastName(customerRequest.getLastName())
                .dateOfBirth(customerRequest.getDateOfBirth())
                .build();
        customerRepository.save(customer);
        return new IdResponse(uuid);
    }

    public void update(final IdentifiedCustomerRequest identifiedCustomerRequest) {
        final Customer customer = customerRepository.findById(identifiedCustomerRequest.getId())
                .map(Customer.class::cast).get();
        CustomerDtoMapper.INSTANCE.updateCustomerFromDto(identifiedCustomerRequest,customer);
        customerRepository.save(customer);
    }

    public void delete(final String customerId) {
        customerRepository.deleteById(customerId);
    }

    @Transactional(readOnly = true)
    public List<Customer> search(final String searchText) {
        Query simpleQueryStringQuery = getQueryBuilder()
                .simpleQueryString()
                .onFields("firstName", "lastName")
                .matching(searchText)
                .createQuery();

        return getJpaQuery(simpleQueryStringQuery).getResultList();
    }

    @Transactional(readOnly = true)
    public List<Customer> getCustomers() {
        return customerRepository.findAll();
    }

    private FullTextQuery getJpaQuery(final Query luceneQuery) {
        final FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);

        return fullTextEntityManager.createFullTextQuery(luceneQuery, Customer.class);
    }

    private QueryBuilder getQueryBuilder() {
        final FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);

        return fullTextEntityManager.getSearchFactory()
                .buildQueryBuilder()
                .forEntity(Customer.class)
                .get();
    }
}
