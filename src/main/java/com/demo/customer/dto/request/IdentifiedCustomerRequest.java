package com.demo.customer.dto.request;

import java.time.LocalDate;

import javax.validation.constraints.NotNull;
import com.demo.customer.dto.AbstractPerson;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

@Getter
@JsonDeserialize(builder = IdentifiedCustomerRequest.IdentifiedCustomerRequestBuilder.class)
public class IdentifiedCustomerRequest extends AbstractPerson {

    @NotNull
    protected final String id;

    @Builder(toBuilder = true)
    public IdentifiedCustomerRequest(final String id, final String firstName, final String lastName,
                                        final LocalDate dateOfBirth) {
        super(firstName, lastName, dateOfBirth);
        this.id = id;
    }

    @JsonPOJOBuilder(withPrefix = StringUtils.EMPTY)
    public static class IdentifiedCustomerRequestBuilder {
    }
}
