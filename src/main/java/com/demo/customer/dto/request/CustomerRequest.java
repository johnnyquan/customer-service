package com.demo.customer.dto.request;

import java.time.LocalDate;

import com.demo.customer.dto.AbstractPerson;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

@Getter
@JsonDeserialize(builder = CustomerRequest.CustomerRequestBuilder.class)
public class CustomerRequest extends AbstractPerson {
    @Builder(toBuilder = true)
    protected CustomerRequest(final String firstName, final String lastName, final LocalDate dateOfBirth) {
        super(firstName, lastName, dateOfBirth);
    }

    @JsonPOJOBuilder(withPrefix = StringUtils.EMPTY)
    public static class CustomerRequestBuilder {
    }
}
