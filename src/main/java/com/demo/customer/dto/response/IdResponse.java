package com.demo.customer.dto.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

@Getter
@JsonDeserialize(builder = IdResponse.IdResponseBuilder.class)
public class IdResponse {

    @ApiModelProperty("The unique identifier of a resource")
    private final String id;

    @Builder(toBuilder = true)
    @JsonCreator
    public IdResponse(@JsonProperty("id") String id) {
        this.id = id;
    }

    @JsonPOJOBuilder(withPrefix = StringUtils.EMPTY)
    public static class IdResponseBuilder {
    }
}
