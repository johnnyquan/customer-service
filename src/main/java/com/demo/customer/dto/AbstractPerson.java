package com.demo.customer.dto;

import java.time.LocalDate;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

@Getter
public abstract class AbstractPerson {

	@ApiModelProperty("The first name of the customer")
	protected final String firstName;

	@ApiModelProperty("The last name of the customer")
	protected final String lastName;

	@ApiModelProperty("The birth date of the customer")
	protected final LocalDate dateOfBirth;

	protected AbstractPerson(final String firstName, final String lastName, final LocalDate dateOfBirth) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.dateOfBirth = dateOfBirth;
	}
}
