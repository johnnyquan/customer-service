package com.demo.customer.dao.entity;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.TermVector;

@Entity
@Table(name = "customers")
@Data
@Indexed
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Customer {
    @Id
    private String id;

    @Field(termVector = TermVector.YES)
    private String firstName;

    @Field(termVector = TermVector.YES)
    private String lastName;

    @Field
    private LocalDate dateOfBirth;
}
