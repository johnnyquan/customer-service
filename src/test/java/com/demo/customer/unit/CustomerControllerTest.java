package com.demo.customer.unit;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.time.LocalDate;

import com.demo.customer.controller.CustomerController;
import com.demo.customer.dto.request.CustomerRequest;
import com.demo.customer.dto.request.IdentifiedCustomerRequest;
import com.demo.customer.service.CustomerService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CustomerControllerTest {

    @InjectMocks
    private CustomerController customerController;

    @Mock
    private CustomerService customerService;

    public static final CustomerRequest mockCustomerRequest = CustomerRequest.builder()
            .dateOfBirth(LocalDate.ofYearDay(1990,1))
            .firstName("John")
            .lastName("Smith").build();

    public static final IdentifiedCustomerRequest identifiedCustomerRequest = IdentifiedCustomerRequest.builder()
            .firstName("Jimmy")
            .lastName("Smith").build();

    @Test
    public void createCustomer_givenValidRequest_succeeds() {
        // When
        customerController.create(mockCustomerRequest);

        // Then
        verify(customerService, times(1)).create(mockCustomerRequest);
    }

    @Test
    public void updateCustomer_givenValidRequest_succeeds() {
        // When
        customerController.update(identifiedCustomerRequest);

        // Then
        verify(customerService, times(1)).update(identifiedCustomerRequest);
    }

    @Test
    public void deleteCustomer_givenValidRequest_succeeds() {
        // When
        customerController.delete("mockeId");

        // Then
        verify(customerService, times(1)).delete("mockeId");
    }

    @Test
    public void searchCustomer_givenValidRequest_succeeds() {
        // When
        customerController.search("John");

        // Then
        verify(customerService, times(1)).search("John");
    }

    @Test
    public void getAllCustomers_givenValidRequest_succeeds() {
        // When
        customerController.getAll();

        // Then
        verify(customerService, times(1)).getCustomers();
    }

}