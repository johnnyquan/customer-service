package com.demo.customer.integration;

import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.nullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.demo.customer.TestApplication;
import com.demo.customer.dao.entity.Customer;
import com.demo.customer.dto.request.CustomerRequest;
import com.demo.customer.dto.request.IdentifiedCustomerRequest;
import com.demo.customer.dto.response.IdResponse;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
@SpringBootTest(classes = TestApplication.class)
public class CustomerIntegrationTest {
    
    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    protected ObjectMapper objectMapper;

    @PersistenceContext
    private EntityManager entityManager;

    private MockMvc mockMvc;

    @BeforeEach
    public void setUp() { mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    public static final String URL_BASE = "/customer";
    public static final String SEARCH_URL = URL_BASE + "/search/{firstNameOrLastName}";
    public static final String DELETE_URL = URL_BASE + "/{customerId}";


    public static final CustomerRequest customerRequest = CustomerRequest.builder()
            .dateOfBirth(LocalDate.ofYearDay(1990,1))
            .firstName("John")
            .lastName("Smith").build();

    // --------------- POST -----------------

    @Test
    public void postCustomer_givenEmptyRequest_shouldReturn400() throws Exception {

        mockMvc.perform(post(URL_BASE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(EMPTY))
                .andExpect(status().isBadRequest())
                .andDo(print())
                .andReturn();
    }

    @Test
    public void postCustomer_givenValidRequest_shouldReturn201() throws Exception {

        final MvcResult result = mockMvc.perform(post(URL_BASE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(customerRequest)))
                .andExpect(status().isCreated())
                .andDo(print())
                .andReturn();

        final IdResponse response = objectMapper.readValue(result.getResponse().getContentAsString(), IdResponse.class);

        assertThat(response.getId(), is(not(nullValue())));
    }

    // --------------- UPDATE/DELETE/SEARCH -----------------
    @Test
    public void editCustomer_givenValidRequest_shouldReturn204() throws Exception {
        final MvcResult result = mockMvc.perform(post(URL_BASE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(customerRequest)))
                .andExpect(status().isCreated())
                .andDo(print())
                .andReturn();

        final IdResponse response = objectMapper.readValue(result.getResponse().getContentAsString(), IdResponse.class);

        assertThat(response.getId(), is(not(nullValue())));

        final IdentifiedCustomerRequest identifiedCustomerRequest = IdentifiedCustomerRequest.builder()
                .id(response.getId())
                .firstName("Jimmy")
                .build();

        mockMvc.perform(put(URL_BASE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(identifiedCustomerRequest)))
                .andExpect(status().isNoContent())
                .andDo(print())
                .andReturn();

        final MvcResult searchResult = mockMvc.perform(get(SEARCH_URL, "Jimmy")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print())
                .andReturn();

        final List<Customer> searchCustomers = objectMapper.readValue(searchResult.getResponse().getContentAsString(),
                new TypeReference<List<Customer>>() {
        });

        assertThat(searchCustomers.get(0).getFirstName(), is("Jimmy"));

        mockMvc.perform(delete(DELETE_URL, searchCustomers.get(0).getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent())
                .andDo(print())
                .andReturn();

    }
}
